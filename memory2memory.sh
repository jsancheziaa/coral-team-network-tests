#!/bin/bash
 
# Values for --window-size
window_sizes=("10k" "50k" "100k" "200k" "250k" "500k" "1000k")
 
# Values for --duration
durations=("PT10S" "PT20S" "PT30S" "PT40S" "PT50S" "PT60S")
 
# Output folder
output_folder="output_files"
 
# Create folder if it does not exist
mkdir -p "$output_folder"
 
# values for --window-size
for size in "${window_sizes[@]}"; do
    # Value of --duration
    for duration in "${durations[@]}"; do
        # name output file
        output_file="$output_folder/output_size_${size}_duration_${duration}.txt"
        # run the command and redirect the output
        docker exec -t <container_name> /bin/bash -c "pscheduler task throughput --window-size=$size --duration=$duration --dest perfsonar01.jc.rl.ac.uk" > "$output_file"
    done
done