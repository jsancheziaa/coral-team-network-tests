#!/bin/bash

# Configuration
SOURCE_DIR="/path/to/source"  # Replace with the source directory
DEST_DIR="/path/to/destination"  # Replace with the destination directory
CSVFILE="data_movement_results_$(date +%Y%m%d_%H%M%S).csv"

# Initialize CSV file with headers
echo "timestamp,file_name,operation,result" > $CSVFILE

# Function to copy files and verify integrity
copy_and_verify() {
    local src_file=$1
    local dest_file=$2
    local file_name=$(basename "$src_file")

    # Copy the file
    cp "$src_file" "$dest_file"
    if [[ $? -ne 0 ]]; then
        echo "$(date),$file_name,copy,failed" >> $CSVFILE
        return 1
    else
        echo "$(date),$file_name,copy,success" >> $CSVFILE
    fi

    # Verify the file integrity using checksums
    src_checksum=$(md5sum "$src_file" | awk '{ print $1 }')
    dest_checksum=$(md5sum "$dest_file" | awk '{ print $1 }')
    if [[ "$src_checksum" == "$dest_checksum" ]]; then
        echo "$(date),$file_name,verify,success" >> $CSVFILE
    else
        echo "$(date),$file_name,verify,failed" >> $CSVFILE
    fi
}

# Function to perform the data movement test
perform_test() {
    echo "Starting data movement test on $(date)" | tee -a $CSVFILE
    for file in "$SOURCE_DIR"/*; do
        if [[ -f $file ]]; then
            copy_and_verify "$file" "$DEST_DIR/$(basename "$file")"
        fi
    done
    echo "Data movement test completed on $(date)" | tee -a $CSVFILE
}

# Execute the test
perform_test