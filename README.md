# Network Test

In this repository we will collect the work done in tasks COR-585 and COR-603.

COR-583: https://confluence.skatelescope.org/display/SRCSC/COR-585+%5BNetwork+tests%5D++Memory+to+memory+tests%3A+Change+the+repo+configuration+if+it+is+needed

COR-603: https://confluence.skatelescope.org/display/SRCSC/COR-603+%5BNetwork+tests%5D+Disk-to-disk+tests

# Tool Used

The tool we used for this purpose is PerfSonar

# How install PerfSonar

We deployed PerfSonar using docker them the fist step is to install docker.
## Docker installation
We are using Ubuntu 20.04 and the show the steps to do this installation

    sudo apt -y update
    sudo apt install -y apt-transport-https ca-certificates     
    curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
    apt-cache policy docker-ce
    sudo apt install -y docker-ce
## Check status
    sudo systemctl status docker
## Executing docker command without sudo
    sudo usermod -aG docker ${USER}


## Pull the PerfSonar docker image
    docker pull perfsonar/testpoint

This will download the latest built image of the perfsonar testpoint bundle. It includes a base CentOS7 install and the perfsonar-testpoint packages. Once the image is downloaded and extracted, start up the container by doing:

    docker run -d --net=host perfsonar/testpoint

## Firewall changes
The next step is configure the firewall, in out case, iptables. The following table show the ports used by perfSONAR (https://www.perfsonar.net/deployment_security.html)

This is our configuration for iptables

    ### perfSONAR Toolkit Firewall Config v1.0
 
    *filter
    :INPUT ACCEPT [0:0]
    :FORWARD ACCEPT [0:0]
    :OUTPUT ACCEPT [0:0]
 
    # convenience for logging things we want to specifically deny
    #-N DENYLOG
    #-A DENYLOG -j LOG --log-prefix DENIED_HOST:
    #-A DENYLOG -j DROP
    #-A INPUT -j DENYLOG -s <someipORnetwork>
 
    # Allow Loopback
    -A INPUT -i lo -j ACCEPT
    -A OUTPUT -o lo -j ACCEPT
 
    # Accept ICMP
    -A INPUT -p icmp --icmp-type any -j ACCEPT
 
    # Accept existing and related connections
    -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
 
    #NTP rules
    -A INPUT -s 0/0 -d 0/0 -p udp --source-port 123:123 -m state --state ESTABLISHED -j ACCEPT
    -A OUTPUT -s 0/0 -d 0/0 -p udp --destination-port 123:123 -m state --state NEW,ESTABLISHED -j ACCEPT
 
    # =-=-=-=-=-=- Core perfSONAR Services =-=-=-=-=-=-
 
    # Incoming Web - TCP Ports 80 and 443
    -A INPUT -m tcp -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
    -A INPUT -m tcp -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT
 
    # Incoming NTP - UDP Port 123
    -A INPUT -p udp --dport 123 -m udp -j ACCEPT
 
    # Incoming SSH - TCP Port 22
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 22 -j ACCEPT
 
    # =-=-=-=-=-=- perfSONAR Measurement Tools =-=-=-=-=-=-
 
    # UDP Traceroute (Incoming)
    -A INPUT -m udp -p udp --dport 33434:33634 -j ACCEPT
 
    # NPAD Control (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8000 -j ACCEPT
 
    # NPAD Test (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8001:8020 -j ACCEPT
 
    # NDT Control (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 7123 -j ACCEPT
 
    # NDT Test (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 3001:3003 -j ACCEPT
 
    # OWAMP Control (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 861 -j ACCEPT
 
    # OWAMP Test (Incoming)
    -A INPUT -m udp -p udp --dport 8760:8960 -j ACCEPT
 
    # BWCTL Control (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 4823 -j ACCEPT
 
    # BWCTL Peer (Incoming, TCP and UDP)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 6001:6200 -j ACCEPT
    -A INPUT -m udp -p udp --dport 6001:6200 -j ACCEPT
 
    # BWCTL Test (Incoming, TCP and UDP)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 5000:5600 -j ACCEPT
    -A INPUT -m udp -p udp --dport 5000:5600 -j ACCEPT
 
    # BWCTL Test [Historic] (Incoming, TCP and UDP)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 10101:10300 -j ACCEPT
    -A INPUT -m udp -p udp --dport 10101:10300 -j ACCEPT
 
    # =-=-=-=-=-=- Measurement Middleware =-=-=-=-=-=-
 
    # perfSONAR gLS Echo (Incoming, TCP and UDP)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 7 -j ACCEPT
    -A INPUT -m udp -p udp --dport 7 -j ACCEPT
 
    # perfSONAR SNMP MA (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8065 -j ACCEPT
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 9990 -j ACCEPT
 
    # perfSONAR perfSONAR-BUOY Web Service (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8085 -j ACCEPT
 
    # perfSONAR Traceroute MA (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8086 -j ACCEPT
 
    # perfSONAR Traceroute MP [Historical] (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8087 -j ACCEPT
 
    # perfSONAR PingER Control and Testing (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8075 -j ACCEPT
 
    # perfSONAR perfSONAR-BUOY Control (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8569:8570 -j ACCEPT
 
    # SSH
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 22:22 -j ACCEPT
 
    # perfSONAR Lookup Service [Historical] (Incoming)
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8090 -j ACCEPT
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 8095 -j ACCEPT
    -A INPUT -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport 9995 -j ACCEPT
 
    # log before we drop whatever is left.
    # -A INPUT -j LOG --log-prefix DROPPED_PACKET:
 
    # Drop the rest
 
    -A INPUT -j REJECT --reject-with icmp-host-prohibited
    -A FORWARD -j REJECT --reject-with icmp-host-prohibited
 
    -I INPUT 1 -m udp -p udp --dport  5001:5300 -j ACCEPT
    -I INPUT 1 -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport  5001:5300 -j ACCEPT
    -I INPUT 1 -m udp -p udp --dport  5301:5600 -j ACCEPT
    -I INPUT 1 -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport  5301:5600 -j ACCEPT
    -I INPUT 1 -m udp -p udp --dport  6001:6200 -j ACCEPT
    -I INPUT 1 -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport  6001:6200 -j ACCEPT
    -I INPUT 1 -m udp -p udp --dport  8760:8960 -j ACCEPT
    -I INPUT 1 -m state --state NEW,ESTABLISHED -m tcp -p tcp --dport  8760:8960 -j ACCEPT
 
    -A OUTPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT
 
 
    COMMIT

# Run test
    docker exec -it <name_or_id_docker_container> /bin/bash -c "pscheduler task throughput --dest perfsonar01.jc.rl.ac.uk"

We need to apply some changes in the vm configuration. This changes enable to use different buffer sizes.

    vi /etc/sysctl.conf

Add following towards bottom

    ### IMPROVE SYSTEM MEMORY MANAGEMENT ###
 
    # Increase size of file handles and inode cache
    fs.file-max = 2097152
 
    # Do less swapping
    vm.swappiness = 10
    vm.dirty_ratio = 60
    vm.dirty_background_ratio = 2
 
    ### GENERAL NETWORK SECURITY OPTIONS ###
 
    # Number of times SYNACKs for passive TCP connection.
    net.ipv4.tcp_synack_retries = 2
 
    # Allowed local port range
    net.ipv4.ip_local_port_range = 2000 65535
 
    # Protect Against TCP Time-Wait
    net.ipv4.tcp_rfc1337 = 1
 
    # Decrease the time default value for tcp_fin_timeout connection
    net.ipv4.tcp_fin_timeout = 15
 
    # Decrease the time default value for connections to keep alive
    net.ipv4.tcp_keepalive_time = 300
    net.ipv4.tcp_keepalive_probes = 5
    net.ipv4.tcp_keepalive_intvl = 15
 
    ### TUNING NETWORK PERFORMANCE ###
 
    # Default Socket Receive Buffer
    net.core.rmem_default = 31457280
 
    # Maximum Socket Receive Buffer
    net.core.rmem_max = 12582912
 
    # Default Socket Send Buffer
    net.core.wmem_default = 31457280
 
    # Maximum Socket Send Buffer
    net.core.wmem_max = 12582912
 
    # Increase number of incoming connections
    net.core.somaxconn = 4096
 
    # Increase number of incoming connections backlog
    net.core.netdev_max_backlog = 65536
 
    # Increase the maximum amount of option memory buffers
    net.core.optmem_max = 25165824
 
    # Increase the maximum total buffer-space allocatable
    # This is measured in units of pages (4096 bytes)
    net.ipv4.tcp_mem = 65536 131072 262144
    net.ipv4.udp_mem = 65536 131072 262144
 
    # Increase the read-buffer space allocatable
    net.ipv4.tcp_rmem = 8192 87380 16777216
    net.ipv4.udp_rmem_min = 16384
 
    # Increase the write-buffer-space allocatable
    net.ipv4.tcp_wmem = 8192 65536 16777216
    net.ipv4.udp_wmem_min = 16384
 
    # Increase the tcp-time-wait buckets pool size to prevent simple DOS attacks
    net.ipv4.tcp_max_tw_buckets = 1440000
    net.ipv4.tcp_tw_recycle = 1
    net.ipv4.tcp_tw_reuse = 1

Load Changes
Run following command to load changes to sysctl.

    sysctl -p
# Scripts
## Memory-to-memory
The first script collects the throughput as a function of window size and test duration. (memory2memory.sh)

The results are exported to text files specifying the window size and the duration of the test.

The second script gets the throughput for different window sizes and for a specific duration. (memory2memory_v2.sh)

## Disk-to-disk
The first script collects the throughput using a simple rsync. (disk2disk.sh)

The second script run the test but collecting the parameters from the command line (disk2disk_v1.sh)

    bash disk_to_disk_v1.sh <size_MB> destination/folder

The third script script collects the throughput using pscheluder and curl. (disk2disk_v2.sh)


## Funtional data movement test


data_movement_test.sh script will copy files from a source directory to a destination directory, verify the integrity of the copied files using checksums, and log the results to a CSV file. This will help you ensure that data is moved correctly without any corruption.

Give execution permissions to the script

    chmod +x data_movement_test.sh
Schedule the script to run automatically with cron

    crontab -e
    0 2 * * * /path/to/script/data_movement_test.sh 

Options:

- Configuration: Defines the source directory, the destination directory, and the CSV file name.
- CSV File Initialization: Initializes the CSV file with headers.
- copy_and_verify: Copies a file from the source to the destination and verifies its integrity using md5sum. Logs the results to the CSV file.
- perform_test: Iterates over each file in the source directory, calling copy_and_verify for each file. Logs the start and end times of the test.
- Execute the test: Calls the perform_test function to start the test.

The destination path can be defined on a ceph fs unit, on a BS unit, use a remote path or a datalake path, for example rucio