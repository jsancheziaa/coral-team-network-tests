#!/bin/bash

# Input and output CSV files
input_csv="output_files/results_filtered.csv"
output_csv="output_files/results_averages.csv"

# Write header to the output CSV file
echo "WindowSize,Duration,AverageThroughput,Retransmits" > "$output_csv"

# Use awk to process and calculate averages
awk -F',' '
NR > 1 {
    # Process WindowSize (column 1)
    window_size = $2
    if (window_size ~ /k$/) {
        window_size = substr(window_size, 1, length(window_size) - 1) / 1000
    } else if (window_size ~ /M$/) {
        window_size = substr(window_size, 1, length(window_size) - 1)
    }

    # Process Throughput and its unit (columns 5 and 6)
    throughput = $5
    unit = $6
    if (unit == "Gbps") {
        throughput *= 1000
        unit = "Mbps"
    }

    # Define key for grouping
    key = window_size FS $3 FS unit  # Group by WindowSize, Duration, and unit (after conversion)

    # Sum the values of throughput and count occurrences
    sum[key] += throughput
    count[key]++
}
END {
    for (k in sum) {
        split(k, a, FS)   # Split the key into individual fields
        avg = sum[k] / count[k]
        print a[1] "," a[2] "," avg "," a[3] >> "'$output_csv'"
    }
}' "$input_csv"

echo "Average calculation completed. Results saved to $output_csv."