#!/bin/bash

# Function to check the status of the previous command and exit if it failed
check_status() {
    if [ $? -ne 0 ]; then
        echo "Error: $1 failed to execute."
        exit 1
    fi
}

# Execute network_monitor.sh and check status
./network_monitor.sh
check_status "network_monitor.sh"

# Execute filter_results.sh and check status
./filter_results.sh
check_status "filter_results.sh"

# Execute calculate_averages.sh and check status
./calculate_averages.sh
check_status "calculate_averages.sh"

# Execute plot_results.sh and check status
./plot_results.sh
check_status "plot_results.sh"

echo "All scripts executed successfully."
