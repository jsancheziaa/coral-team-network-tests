#!/bin/bash

# Check if gnuplot is installed, if not, install it
if ! command -v gnuplot &> /dev/null
then
    echo "gnuplot could not be found, installing..."
    sudo apt-get update
    sudo apt-get install -y gnuplot
fi

# Input CSV file
input_csv="output_files/results_averages_backup.csv"
# Output PNG file
output_png="output_files/results_plot_backup.png"

# Generate gnuplot script
gnuplot_script=$(mktemp)

cat << EOF > "$gnuplot_script"
set datafile separator ","
set terminal png size 800,600
set output "$output_png"
set title "Average Throughput vs Window Size"
set xlabel "Window Size (k or M)"
set ylabel "Average Throughput (Mbps)"
set grid
plot "$input_csv" using 1:3 with linespoints title "Average Throughput"
EOF

# Run gnuplot with the generated script
gnuplot "$gnuplot_script"

# Clean up
rm "$gnuplot_script"

echo "Plot generated and saved to $output_png."