#!/bin/bash

# Values for --window-size
window_sizes=("10k" "50k" "100k" "200k" "250k" "500k" "1000k" "1M" "5M" "10M" "20M")

# Values for --duration (in seconds)
durations=("PT0S" "PT10S" "PT20S" "PT30S" "PT40S" "PT50S")

# Folder to store the output files
output_folder="output_files"

# Create the output folder if it doesn't exist
mkdir -p "$output_folder"

# CSV file to store the filtered results
csv_file="$output_folder/results_summary.csv"

# Write the header in the CSV file
echo "TestNum,WindowSize,Duration,Interval,Throughput,Retransmits,ReceiverThroughput" > "$csv_file"

# Function to process the output file and extract the summary
process_output() {
    local output_file=$1
    local test_num=$2
    local size=$3
    local duration=$4

    # Search and extract the summary text
    local summary=$(grep -A 2 "Summary" "$output_file" | tail -n 1)

    if [ -n "$summary" ]; then
        # Extract values from the summary
        local interval=$(echo "$summary" | awk '{print $1" "$2}')
        local throughput=$(echo "$summary" | awk '{print $3" "$4}')
        local retransmits=$(echo "$summary" | awk '{print $5}')
        local receiver_throughput=$(echo "$summary" | awk '{print $6" "$7}')

        # Save the results to the CSV file
        echo "$test_num,$size,$duration,$interval,$throughput,$retransmits,$receiver_throughput" >> "$csv_file"
    fi
}

# Iterate over the values for --window-size
for size in "${window_sizes[@]}"; do
    # Iterate over the values for --duration
    for duration in "${durations[@]}"; do
        # Perform 10 executions for each combination of window_size and duration
        for i in {1..10}; do
            # Format the execution number with two digits
            test_num=$(printf "%02d" $i)
            # Output file name
            output_file="$output_folder/output_size_${size}_duration_${duration}_run_${test_num}.txt"
            # Execute the docker command and redirect the output to the file
            docker exec -t espsrc_perfsonar /bin/bash -c "pscheduler task throughput --window-size=$size --duration=$duration --dest perfsonar01.jc.rl.ac.uk" > "$output_file"

            # Check if the output file has data
            if [ -s "$output_file" ]; then
                echo "Execution $test_num for window size $size and duration $duration completed successfully and data is present."
                # Process the output file to extract and save the summary
                process_output "$output_file" "$test_num" "$size" "$duration"
            else
                echo "Execution $test_num for window size $size and duration $duration failed or the output file is empty."
            fi
        done
    done
done
