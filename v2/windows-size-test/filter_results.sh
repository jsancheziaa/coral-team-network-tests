#!/bin/bash

# Original CSV file
input_csv="output_files/results_summary.csv"
# Filtered CSV file
output_csv="output_files/results_filtered.csv"

# Write header to the filtered CSV file
echo "TestNum,WindowSize,Duration,Throughput,ThroughputUnit,Retransmits" > "$output_csv"

# Read the original CSV file line by line
tail -n +2 "$input_csv" | while IFS=',' read -r test_num window_size duration interval throughput retransmits receiver_throughput; do
    # Extract throughput value and unit
    throughput_value=$(echo "$throughput" | awk '{print $1}')
    throughput_unit=$(echo "$throughput" | awk '{print $2}')

    # Save filtered results to the new CSV file
    echo "$test_num,$window_size,$duration,$throughput_value,$throughput_unit,$retransmits" >> "$output_csv"
done

echo "Filtering and modification completed. Results saved to $output_csv."
