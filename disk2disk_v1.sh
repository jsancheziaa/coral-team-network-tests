#!/bin/bash

# Check if all arguments are provided
if [ $# -ne 2 ]; then
    echo "Usage: $0 FILE_SIZE_MB TARGET"
    exit 1
fi

# Assign arguments to variables
FILE_SIZE_MB=$1
TARGET=$2

# Configuration
LOGFILE="log_copy.txt"

# Function to generate a test file
generate_test_file() {
    local filename=$1
    local size_mb=$2
    echo "Starting file creation ..."
    dd if=/dev/urandom of=$filename bs=1M count=$size_mb
    echo "File creation completed"
}

# Function to measure the rsync process duration and calculate throughput
test_copy() {
    local source_file=$1
    local target_dir=$2
    local file_size_mb=$3

    echo "Starting copy test from $source_file to $target_dir..."

    # Remove previous content if it exists
    rm -rf $target_dir/*
    mkdir -p $target_dir

    # Get the total size of the file
    TOTAL_SIZE=$(stat --format="%s" $source_file)

    # Start time
    START_TIME=$(date +%s)

    # Run rsync process
    rsync -avh --progress $source_file $target_dir/

    # End time
    END_TIME=$(date +%s)

    # Calculate elapsed time
    ELAPSED_TIME=$((END_TIME - START_TIME))

    # Calculate throughput (bytes per second)
    if [ $ELAPSED_TIME -gt 0 ]; then
        THROUGHPUT=$((TOTAL_SIZE / ELAPSED_TIME))
    else
        THROUGHPUT=0
    fi

    # Convert throughput to MB/s for clarity
    THROUGHPUT_MB=$(echo "scale=2; $THROUGHPUT / 1024 / 1024" | bc)

    echo "Copy test finished in $ELAPSED_TIME seconds."
    echo "Throughput: $THROUGHPUT_MB MB/s."

    # Log results
    echo "File size: $file_size_mb MB." >> $LOGFILE
    echo "Copy test finished in $ELAPSED_TIME seconds." >> $LOGFILE
    echo "Throughput: $THROUGHPUT_MB MB/s." >> $LOGFILE
    echo "==============================================" >> $LOGFILE
}

# Create the test file
TEST_FILE="test_file_${FILE_SIZE_MB}MB.dat"
generate_test_file $TEST_FILE $FILE_SIZE_MB

# Run the copy test
test_copy $TEST_FILE $TARGET $FILE_SIZE_MB

# Clean up the test file
rm -f $TEST_FILE
rm -f $TARGET/$(basename $TEST_FILE)

echo "Test completed. Results saved in $LOGFILE"
