#!/bin/bash

# Docker container name
DOCKER_CONTAINER="<container_name_pscheluder_curl>"
# Output file
RESULT_FILE="throughput_results.txt"
# Source 
SOURCE="ftp://speedtest.tele2.net/1KB.zip"
# Target
TARGET="/tmp/test.out"
TIMEOUT="PT5S"

# Function to show script usage
usage() {
  echo "USAGE: $0 -s <tamaño_fichero> -d <destino> -o <archivo_resultados>"
  exit 1
}

# Process script arguments
while getopts ":s:d:o:" opt; do
  case $opt in
    s)
      FILE_SIZE="$OPTARG"
      ;;
    d)
      DEST="$OPTARG"
      ;;
    o)
      RESULT_FILE="$OPTARG"
      ;;
    *)
      usage
      ;;
  esac
done

# Verify that all necessary arguments have been provided.
if [ -z "$FILE_SIZE" ] || [ -z "$DEST" ] || [ -z "$RESULT_FILE" ]; then
  usage
fi

# Create a test file with specified size
TEST_FILE="test_file"
dd if=/dev/urandom of=$TEST_FILE bs=1M count=$FILE_SIZE

# Run the command pscheduler task throughput in the Docker container and redirect the results to the file
docker exec -t $DOCKER_CONTAINER /bin/bash -c "pscheduler task --tool curl disk-to-disk --source $SOURCE --dest $TARGET --timeout $TIMEOUT" > $RESULT_FILE

# Clean up the test file
rm -f $TEST_FILE

echo "Test  throughput finished. Results stored in $RESULT_FILE"
