#!/bin/bash
 
# Values for --window-size
window_sizes=("10k" "50k" "100k" "200k" "250k" "500k" "1000k" "1M" "5M" "10M" "20M")
 
#  Values for --duration
durations=("PT10S")
# Output folder
output_folder="output_files_10s"
 
# Create folder if it does not exist
mkdir -p "$output_folder"
 
# values for --window-size
for size in "${window_sizes[@]}"; do
    # Rvalues for --duration
    for duration in "${durations[@]}"; do
        # name output file
        output_file="$output_folder/output_size_${size}_duration_${duration}.txt"
        # run the command and redirect the output
        docker exec -t espsrc_perfsonar /bin/bash -c "pscheduler task throughput --window-size=$size --duration=$duration --dest perfsonar01.jc.rl.ac.uk" > "$output_file"
    done
done